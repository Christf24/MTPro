package mtproproject.mtpro;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by christophe on 4/2/15.
 *
 * Vehicle model class
 */
public class VehicleList {

    private ArrayList<Vehicle> mVehicles;
    private ArrayList<Fillup> mFillups;
    private static VehicleList sVehicleList;
    public Context mAppContext;
    private static final String TAG = "VehicleList";
    private static final String FILENAME = "vehicles.json";
    private VehicleIntentJSONSerializer mSerializer;

    private VehicleList(Context appContext)
    {
        mAppContext = appContext;
        mVehicles = new ArrayList<Vehicle>();
        mFillups = new ArrayList<Fillup>();
        mSerializer = new VehicleIntentJSONSerializer(mAppContext, FILENAME);
        try {
            mVehicles = mSerializer.loadVehicles();
            mFillups = mSerializer.loadFillups();
        } catch (Exception e) {
            mVehicles = new ArrayList<Vehicle>();
            mFillups = new ArrayList<Fillup>();
            Log.e(TAG, "Error loading vehicles");
        }
        mSerializer = new VehicleIntentJSONSerializer(mAppContext, FILENAME);
    }

    public static VehicleList get(Context c)
    {
        if (sVehicleList == null)
            sVehicleList = new VehicleList(c.getApplicationContext());

        return sVehicleList;
    }
    public ArrayList<Vehicle> getVehicles(){ return mVehicles; }

    public ArrayList<Fillup> getFillups(){ return mFillups; }

    public ArrayList<Fillup> getFillupsForVehicle(UUID id)
    {
        ArrayList<Fillup> fillupForVehicle = new ArrayList<>();

        for (Fillup f: mFillups)
        {
            if ( f.getID().equals(id) )
            {
                fillupForVehicle.add(f);
            }

        }
        if ( ! fillupForVehicle.isEmpty())
            return fillupForVehicle;

        return null;
    }

    public Vehicle getVehicle(UUID id)
    {
        for (Vehicle v : mVehicles)
        {
            if ( v.getID().equals(id) )
                return v;
        }
        return null;
    }

    public void addVehicle(Vehicle v)
    {
        mVehicles.add(v);
    }

    public void addFillup(Fillup f) { mFillups.add(f); }

    public boolean saveVehicle(ArrayList<Vehicle> Vehicle)
    {
        try {
            mVehicles = Vehicle;
            mSerializer.saveVehicle(Vehicle);
            return true;
        } catch (Exception e)
        {
            return false;
        }
    }

    public boolean saveFillups()
    {
        try {
            mSerializer.saveFillups(mFillups);
            return true;
        } catch (Exception e)
        {
            return false;
        }
    }

    public boolean saveVehicles()
    {
        try {
            mSerializer.saveVehicle(mVehicles);
            return true;
        } catch (Exception e)
        {
            return false;
        }
    }
}
