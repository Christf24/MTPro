package mtproproject.mtpro;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

/**
 * Created by Christf on 4/12/2015.
 */
public class VehicleIntentJSONSerializer {

    private Context mContext;
    private String mFileName;
    private String mFillupFile = "fillups.json";

    public VehicleIntentJSONSerializer(Context context, String file)
    {
        mContext = context;
        mFileName = file;
    }

    public void saveVehicle(ArrayList<Vehicle> vehicles) throws JSONException, IOException {
        JSONArray array = new JSONArray();

        for (Vehicle vehicle : vehicles) {
            array.put(vehicle.toJSON());
        }

        Writer writer = null;

        try {
            OutputStream out = mContext.openFileOutput(mFileName, Context.MODE_PRIVATE);
            writer = new OutputStreamWriter(out);
            writer.write(array.toString());
        } finally {
            if (writer != null)
                writer.close();
        }
    }

    public void saveFillups(ArrayList<Fillup> fillup) throws JSONException, IOException {
        JSONArray array = new JSONArray();

        for (Fillup f : fillup) {
            array.put(f.toJSON());
        }

        Writer writer = null;

        try {
            OutputStream out = mContext.openFileOutput(mFillupFile, Context.MODE_PRIVATE);
            writer = new OutputStreamWriter(out);
            writer.write(array.toString());
        } finally {
            if (writer != null)
                writer.close();
        }
    }

    public ArrayList<Vehicle> loadVehicles() throws IOException, JSONException
    {
        ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
        BufferedReader reader = null;

        try {
            // Open and read the file into a StringBuilder
            InputStream in = mContext.openFileInput(mFileName);
            reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder jsonString = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null)
            {
                jsonString.append(line);
            }

            // parse JSON using JSONTokener
            JSONArray array = (JSONArray) new JSONTokener(jsonString.toString()).nextValue();

            // Build the array of crimes from JSONObject;
            for ( int i = 0; i < array.length(); i++)
            {
                vehicles.add(new Vehicle(array.getJSONObject(i)));
            }
        } catch (FileNotFoundException e) {
            // we will ignore this one since it happens when we start fresh
        } finally {
            if (reader != null)
                reader.close();
        }
        return vehicles;
    }

    public ArrayList<Fillup> loadFillups() throws IOException, JSONException
    {
        ArrayList<Fillup> fillup = new ArrayList<Fillup>();
        BufferedReader reader = null;

        try {
            // Open and read the file into a StringBuilder
            InputStream in = mContext.openFileInput(mFillupFile);
            reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder jsonString = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null)
            {
                jsonString.append(line);
            }

            // parse JSON using JSONTokener
            JSONArray array = (JSONArray) new JSONTokener(jsonString.toString()).nextValue();

            // Build the array of crimes from JSONObject;
            for ( int i = 0; i < array.length(); i++)
            {
                fillup.add(new Fillup(array.getJSONObject(i)));
            }
        } catch (FileNotFoundException e) {
            // we will ignore this one since it happens when we start fresh
        } finally {
            if (reader != null)
                reader.close();
        }
        return fillup;
    }
}
