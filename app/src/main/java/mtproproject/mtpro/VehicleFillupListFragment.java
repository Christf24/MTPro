package mtproproject.mtpro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Christf on 4/20/2015.
 */
public class VehicleFillupListFragment extends ListFragment {

    private UUID vehicleID;

    private ArrayList<Fillup> mFillups;
    private FillupAdapter adapter;

    public static final String EXTRA_VEHICLE_ID = "vehicle.id";

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        // Get ID
        vehicleID = (UUID)getArguments().getSerializable(EXTRA_VEHICLE_ID);

        // Fetch fillups with ID
        mFillups = VehicleList.get(getActivity()).getFillupsForVehicle(vehicleID);

        adapter = new FillupAdapter(mFillups);

        setListAdapter(adapter);
    }

    private class FillupAdapter extends ArrayAdapter<Fillup>
    {
        public FillupAdapter(ArrayList<Fillup> fillups)
        {
            super(getActivity(), 0, fillups);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            // If we weren't given a view, inflate one
            if ( null == convertView )
                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item_fillups, null);

            Fillup fillup = getItem(position);

            TextView dateView = (TextView)convertView.findViewById(R.id.vehicle_fillup_date);
            dateView.setText(fillup.getDate());

            TextView vehicleFillupDistance = (TextView)convertView.findViewById(R.id.vehicle_fillup_distance);
            vehicleFillupDistance.setText(fillup.getDistance());

            TextView vehicleFillupMPG = (TextView)convertView.findViewById(R.id.vehicle_fillup_mpg);
            vehicleFillupMPG.setText(fillup.getMPG());

            return convertView;
        }
    }

    public static VehicleFillupListFragment newInstance(UUID vehicleId)
    {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_VEHICLE_ID, vehicleId);

        VehicleFillupListFragment fragment = new VehicleFillupListFragment();
        fragment.setArguments(args);

        return fragment;
    }
}
