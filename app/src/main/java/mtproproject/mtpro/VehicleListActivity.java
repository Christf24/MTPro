package mtproproject.mtpro;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import java.util.ArrayList;

/**
 * Created by christophe on 4/2/15.
 */
public class VehicleListActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment()
    {
        return new VehicleListFragment();
    }
}
