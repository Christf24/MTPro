package mtproproject.mtpro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Christf on 4/6/2015.
 */
public class AddVehicleFragment extends Fragment {
    private Vehicle new_vehicle;
//    private ArrayList<Vehicle> mVehicles = new ArrayList<>();
    private static String EXTRA_VEHICLE_ID = "EXTRA_VEHICLE_ID";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.add_car, container, false);

        Button add_vehicle = (Button) view.findViewById(R.id.button_add_vehicle);
        add_vehicle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                EditText name = (EditText) view.findViewById(R.id.add_car_name_field);
                EditText year = (EditText) view.findViewById(R.id.add_car_year_field);
                EditText make = (EditText) view.findViewById(R.id.add_car_make_field);
                EditText model = (EditText) view.findViewById(R.id.add_car_model_field);
                EditText trim = (EditText) view.findViewById(R.id.add_car_trim_field);

                // Create a new vehicle object
                new_vehicle = new Vehicle();
                // Set its properties
                new_vehicle.setName(name.getText().toString());
                new_vehicle.setYear(year.getText().toString());
                new_vehicle.setMake(make.getText().toString());
                new_vehicle.setModel(model.getText().toString());
                new_vehicle.setTrim(trim.getText().toString());

                VehicleList.get(getActivity()).addVehicle(new_vehicle);

//                mVehicles = VehicleList.get(getActivity()).getVehicles();

//                mVehicles.add(new_vehicle);
                // Add vehicle to our ArrayList
                VehicleList.get(getActivity()).saveVehicles();

                // Load new activity with vehicle info
                Intent i = new Intent(getActivity(), VehiclePageActivity.class);
                i.putExtra(VehiclePageFragment.EXTRA_VEHICLE_ID, new_vehicle.getID() );

                // Start the activity
                startActivity(i);
            }
        });

        return view;
    }
}
