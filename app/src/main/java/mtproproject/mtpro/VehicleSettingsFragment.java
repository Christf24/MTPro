package mtproproject.mtpro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.UUID;

/**
 * Created by Christf on 4/18/2015.
 */
public class VehicleSettingsFragment extends Fragment {

    private Vehicle vehicle;
    private UUID vehicleID;

    public static final String EXTRA_VEHICLE_ID = "vehicle.id";
    private TextView nameView;
    private TextView vehicleInfo;
    private TextView distance;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        // Get ID
        vehicleID = (UUID)getArguments().getSerializable(EXTRA_VEHICLE_ID);
        // Fetch vehicle with ID
        vehicle = VehicleList.get(getActivity()).getVehicle(vehicleID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.activity_vehicle_settings_page, container, false);

        nameView = (TextView) view.findViewById(R.id.vehicle_page_name);
        nameView.setText(vehicle.getName());

        vehicleInfo = (TextView) view.findViewById(R.id.vehicle_page_info);
        vehicleInfo.setText(vehicle.toString());

        distance = (TextView) view.findViewById(R.id.vehicle_page_mpg);
        distance.setText(vehicle.getMPG());

        Button viewFillups = (Button) view.findViewById(R.id.button_view_fillups);
        viewFillups.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), VehicleFillupListActivity.class);

                i.putExtra(VehicleFillupListFragment.EXTRA_VEHICLE_ID, vehicleID );

                startActivity(i);
            }
        });

        Button clearFillups = (Button) view.findViewById(R.id.button_clear_fillups);
        clearFillups.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Reset all fill-up data
                vehicle.resetFillups();

                // Save updated info
                VehicleList.get(getActivity()).saveVehicles();

                // Reflect change in view
                distance.setText(vehicle.getMPG());
            }
        });

        Button viewVehicles = (Button) view.findViewById(R.id.button_add_vehicle);
        viewVehicles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), VehicleListActivity.class);

                startActivity(i);
            }
        });

        return view;
    }


    public static VehicleSettingsFragment newInstance(UUID vehicleId)
    {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_VEHICLE_ID, vehicleId);

        VehicleSettingsFragment fragment = new VehicleSettingsFragment();
        fragment.setArguments(args);

        return fragment;
    }
}
