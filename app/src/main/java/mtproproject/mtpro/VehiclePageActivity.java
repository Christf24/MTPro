package mtproproject.mtpro;

import android.support.v4.app.Fragment;

import java.util.UUID;

/**
 * Created by christophe on 4/7/15.
 */
public class VehiclePageActivity extends SingleFragmentActivity
{
    @Override
    protected Fragment createFragment()
    {
        UUID vehicleId = (UUID)getIntent().getSerializableExtra(VehiclePageFragment.EXTRA_VEHICLE_ID);

        return VehiclePageFragment.newInstance(vehicleId);
    }
}
