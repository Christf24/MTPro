package mtproproject.mtpro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Christf on 4/18/2015.
 */
public class VehicleAddFillupFragment extends Fragment {

    private Vehicle vehicle;
    private Fillup fillup;
    private UUID vehicleID;

    public static final String EXTRA_VEHICLE_ID = "vehicle.id";

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        // Get ID
        vehicleID = (UUID)getArguments().getSerializable(EXTRA_VEHICLE_ID);
        // Fetch vehicle with ID
        vehicle = VehicleList.get(getActivity()).getVehicle(vehicleID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.add_fillup, container, false);

        Button save = (Button) view.findViewById(R.id.button_add_fillup);

        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText distanceET = (EditText) view.findViewById(R.id.add_fillup_distance);
                EditText gallonET = (EditText) view.findViewById(R.id.add_fillup_gallon);
                EditText priceET = (EditText) view.findViewById(R.id.add_fillup_price);
                EditText dateET = (EditText) view.findViewById(R.id.add_fillup_date);

                Editable distance = distanceET.getText();
                Editable gallon = gallonET.getText();
                Editable price = priceET.getText();
                Editable date = dateET.getText();

                Double g = Double.parseDouble(gallon.toString());
                Double d = Double.parseDouble(distance.toString());
                // Calculate miles per gallon by dividing distance by gallons
                Double mPGallon = d / g;
                mPGallon = (double) Math.round(mPGallon * 100) / 100;

                //calculate total price
                Double ppg = Double.parseDouble(price.toString());
                Double total = (double) Math.round(ppg * g) * 100/100;

                // Convert the price per gallon to string
                String milesPerGallon = Double.toString(mPGallon);

                vehicle.setDistance(distance.toString());
                vehicle.setFuelAmount(gallon.toString());
                vehicle.setPrice(price.toString());
                vehicle.setTotal(total.toString());
                vehicle.setMPG(milesPerGallon);
                vehicle.setFillupDate(date.toString());

                // Add fillup
                fillup = new Fillup();
                fillup.addFillup(vehicle.getID(), vehicle.getFillupDate(), vehicle.getDistance(), vehicle.getMPG());

                VehicleList.get(getActivity()).addFillup(fillup);

                // Save all fillups
                VehicleList.get(getActivity()).saveFillups();

                // Save all vehicles
                VehicleList.get(getActivity()).saveVehicles();

                // Initialize an intent
                Intent i = new Intent(getActivity(), VehiclePageActivity.class);

                // Send ID to the new activity
                i.putExtra(VehiclePageFragment.EXTRA_VEHICLE_ID, vehicle.getID());

                // Begin the activity
                startActivity(i);
            }
        });

        return view;
    }

    public static VehicleAddFillupFragment newInstance(UUID vehicleId)
    {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_VEHICLE_ID, vehicleId);

        VehicleAddFillupFragment fragment = new VehicleAddFillupFragment();
        fragment.setArguments(args);

        return fragment;
    }
}

