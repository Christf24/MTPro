package mtproproject.mtpro;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by christophe on 4/2/15.
 */
public class VehicleListFragment extends ListFragment {

    private ArrayList<Vehicle> mVehicles;
    private VehiclesAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        getActivity().setTitle(R.string.vehicles_title);

        // Fetch list of vehicles
        mVehicles = VehicleList.get(getActivity()).getVehicles();

        if ( ( mVehicles == null ) || ( mVehicles.isEmpty() ) )
        {
            Intent i = new Intent(getActivity(), AddVehicleActivity.class);
            startActivity(i);
        }

        adapter = new VehiclesAdapter(mVehicles);

        setListAdapter(adapter);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Listen for changes in the list
        adapter.notifyDataSetChanged();
    }

    private class VehiclesAdapter extends ArrayAdapter<Vehicle>
    {
        public VehiclesAdapter(ArrayList<Vehicle> vehicles)
        {
            super(getActivity(), 0, vehicles);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            // If we weren't given a view, inflate one
            if ( null == convertView )
                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item_vehicles, null);

            Vehicle vehicle = getItem(position);

            TextView nameView = (TextView)convertView.findViewById(R.id.vehicle_name_nameTextView);
            nameView.setText(vehicle.getName());

            TextView vehicleTextView = (TextView)convertView.findViewById(R.id.vehicle_vehicle_vehicleTextView);
            vehicleTextView.setText(vehicle.toString());

            return convertView;
        }
    }

    @Override
    public void onListItemClick(ListView listview, View view, int position, long id)
    {
        // Get vehicle at clicked position
        Vehicle vehicle = (Vehicle)(getListAdapter()).getItem(position);

        // Initialize an intent
        Intent i = new Intent(getActivity(), VehiclePageActivity.class);

        // Send ID to the new activity
        i.putExtra(VehiclePageFragment.EXTRA_VEHICLE_ID, vehicle.getID());

        // Begin the activity
        startActivity(i);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_add_vehicle, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.menu_item_add_car:
                // Initialize an intent
                Intent i = new Intent(getActivity(), AddVehicleActivity.class);

                // Begin the activity
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
