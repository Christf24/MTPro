package mtproproject.mtpro;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.FragmentActivity;

/**
 * Created by Christf on 4/6/2015.
 */
public class AddVehicleActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment()
        {
            return new AddVehicleFragment();
        }
}
