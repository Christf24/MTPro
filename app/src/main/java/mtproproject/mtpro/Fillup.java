package mtproproject.mtpro;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

/**
 * Created by Christf on 4/20/2015.
 */
public class Fillup {

    private UUID id;
    private String date;
    private String distance;
    private String mpg;

    private static final String JSON_ID = "id";
    private static final String JSON_DATE = "date";
    private static final String JSON_DISTANCE = "distance";
    private static final String JSON_MPG = "mpg";

    public Fillup()
    {

    }

    public Fillup(JSONObject json) throws JSONException {
        id = UUID.fromString(json.getString(JSON_ID));
        date = json.getString(JSON_DATE);
        distance = json.getString(JSON_DISTANCE);
        mpg = json.getString(JSON_MPG);
    }

    public UUID getID() { return id; }

    public String getDate() { return date; }

    public String getDistance() { return distance; }

    public String getMPG() { return mpg; }

    public void addFillup(UUID id, String date, String distance, String mpg)
    {
        this.id = id;
        this.date = date;
        this.distance = distance;
        this.mpg = mpg;
    }

    @Override
    public String toString()
    {
        return date + " " + distance + " " + mpg;
    }

    public JSONObject toJSON() throws JSONException
    {
        JSONObject json = new JSONObject();
        json.put(JSON_ID, id.toString());
        json.put(JSON_DATE, date.toString());
        json.put(JSON_DISTANCE, distance.toString());
        json.put(JSON_MPG, mpg.toString());
        return json;
    }
}
