package mtproproject.mtpro;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.UUID;


public class VehiclePageFragment extends Fragment {

        private Vehicle vehicle;
        private UUID vehicleID;
        private ImageButton mAddButton;
        public static final String EXTRA_VEHICLE_ID = "vehicle.id";

        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);

            // Get ID
            vehicleID = (UUID)getArguments().getSerializable(EXTRA_VEHICLE_ID);
            // Fetch vehicle with ID
            vehicle = VehicleList.get(getActivity()).getVehicle(vehicleID);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            final View view = inflater.inflate(R.layout.fragment_main, container, false);

            TextView nameView = (TextView) view.findViewById(R.id.vehicle_name);
            nameView.setText(vehicle.getName());

            TextView fillupDate = (TextView) view.findViewById(R.id.vehicle_last_fillup_date);
            String fillup = "Last Fillup on ";
            fillupDate.setText(fillup + vehicle.getFillupDate());

            TextView distance = (TextView) view.findViewById(R.id.vehicle_distance);
            String milesUnit = " mi";
            distance.setText(vehicle.getDistance() + milesUnit);

            TextView fuelAmount = (TextView) view.findViewById(R.id.vehicle_fuel_amount);
            String galUnit = " gal";
            fuelAmount.setText(vehicle.getFuelAmount() + galUnit);

            TextView mpg = (TextView) view.findViewById(R.id.vehicle_mpg);
            mpg.setText(vehicle.getMPG());

            TextView pricePerGallon = (TextView) view.findViewById(R.id.vehicle_price);
            String dollarUnit = "$";
            pricePerGallon.setText(dollarUnit + vehicle.getPrice());

            TextView total = (TextView) view.findViewById(R.id.vehicle_total);
            total.setText(dollarUnit + vehicle.getTotal());

            mAddButton = (ImageButton) view.findViewById(R.id.addbutton);
            mAddButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v){
                    Intent i = new Intent(getActivity(), VehicleAddFillupActivity.class);

                    // Send ID to the new activity
                    i.putExtra(VehicleAddFillupFragment.EXTRA_VEHICLE_ID, vehicleID);

                    // Begin the activity
                    startActivity(i);
                }
            });

            return view;
        }

    public static VehiclePageFragment newInstance(UUID vehicleId)
    {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_VEHICLE_ID, vehicleId);

        VehiclePageFragment fragment = new VehiclePageFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_vehicle_page_fragment, menu);
        inflater.inflate(R.menu.menu_vehicle_add_fillup, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.menu_item_vehicle_settings:
                // Initialize an intent
                Intent i = new Intent(getActivity(), VehicleSettingsActivity.class);

                // Send ID to the new activity
                i.putExtra(VehicleSettingsFragment.EXTRA_VEHICLE_ID, vehicleID);

                // Begin the activity
                startActivity(i);
                return true;
            case R.id.menu_item_add_fillup:
                // Initialize an intent
                i = new Intent(getActivity(), VehicleAddFillupActivity.class);

                // Send ID to the new activity
                i.putExtra(VehicleAddFillupFragment.EXTRA_VEHICLE_ID, vehicleID);

                // Begin the activity
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

