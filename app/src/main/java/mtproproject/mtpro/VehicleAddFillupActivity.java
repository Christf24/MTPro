package mtproproject.mtpro;

import android.support.v4.app.Fragment;

import java.util.UUID;

/**
 * Created by Christf on 4/18/2015.
 */
public class VehicleAddFillupActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        UUID vehicleId = (UUID) getIntent().getSerializableExtra(VehicleAddFillupFragment.EXTRA_VEHICLE_ID);

        return VehicleAddFillupFragment.newInstance(vehicleId);
    }

}
