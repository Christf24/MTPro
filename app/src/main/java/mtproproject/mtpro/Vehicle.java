package mtproproject.mtpro;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by christophe on 4/2/15.
 */
public class Vehicle {

    private UUID id;
    private String name;
    private String year;
    private String make;
    private String model;
    private String trim;
    private String fillup_date = "";
    private String distance = "";
    private String fuel_amount = "";
    private String vehicle_mpg = "";
    private String vehicle_price = "";
    private String vehicle_total = "";

//    private Map<Integer, List<String>> fillupList = new HashMap<Integer, List<String>>();

    private static final String JSON_ID = "id";
    private static final String JSON_NAME = "name";
    private static final String JSON_YEAR = "year";
    private static final String JSON_MAKE = "make";
    private static final String JSON_MODEL = "model";
    private static final String JSON_TRIM = "trim";
    private static final String JSON_FILLUP = "fillup";
    private static final String JSON_DISTANCE = "distance";
    private static final String JSON_FUEL_AMT = "fuel_amt";
    private static final String JSON_MPG = "mpg";
    private static final String JSON_PRICE = "price";
    private static final String JSON_TOTAL = "total";
//    private static final String JSON_FILLUPLIST = "filluplist";

    public Vehicle()
    {
        this.id = UUID.randomUUID();
    }

    public Vehicle(JSONObject json) throws JSONException {
        id = UUID.fromString(json.getString(JSON_ID));
        name = json.getString(JSON_NAME);
        year = json.getString(JSON_YEAR);
        make = json.getString(JSON_MAKE);
        model = json.getString(JSON_MODEL);
        trim = json.getString(JSON_TRIM);
        fillup_date = json.getString(JSON_FILLUP);
        distance = json.getString(JSON_DISTANCE);
        fuel_amount = json.getString(JSON_FUEL_AMT);
        vehicle_mpg = json.getString(JSON_MPG);
        vehicle_price = json.getString(JSON_PRICE);
    }

    public UUID getID()
    {
        return this.id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setYear(String year) { this.year = year; }

    public String getYear()
    {
        return year;
    }

    public void setMake(String make) { this.make = make; }

    public String getMake()
    {
        return make;
    }

    public void setModel(String model) { this.model = model; }

    public String getModel()
    {
        return model;
    }

    public void setTrim(String trim) { this.trim = trim; }

    public String getTrim() { return trim; }

    public void setFillupDate(String fillup) { this.fillup_date = fillup; }

    public String getFillupDate() { return fillup_date; }

    public void setDistance(String dist) { this.distance = dist; }

    public String getDistance() { return distance; }

    public void setFuelAmount(String amt) { this.fuel_amount = amt; }

    public String getFuelAmount() { return fuel_amount; }

    public void setMPG(String mpg) { this.vehicle_mpg = mpg; }

    public String getMPG() { return vehicle_mpg; }

    public void setPrice(String price) { this.vehicle_price = price; }

    public String getPrice() { return vehicle_price; }

    public String getTotal() { return vehicle_total; }

    public void setTotal(String price) { this.vehicle_total = price; }

//    public Map<String, List> addFillUp()
//    {
//        List<String> list = new ArrayList<>();
//        list.add(fillup_date, distance, vehicle_mpg);
//        fillupList.add(id, );
//    }

    public void resetFillups()
    {
        fillup_date = "";
        distance = "";
        fuel_amount = "";
        vehicle_mpg = "";
        vehicle_price = "";
        vehicle_total = "";
    }

    @Override
    public String toString()
    {
        return this.year + " " + this.make + " " + this.model;
    }

    public JSONObject toJSON() throws JSONException
    {
        JSONObject json = new JSONObject();
        json.put(JSON_ID, id.toString());
        json.put(JSON_NAME, name.toString());
        json.put(JSON_YEAR, year.toString());
        json.put(JSON_MAKE, make.toString());
        json.put(JSON_MODEL, model.toString());
        json.put(JSON_TRIM, trim.toString());
        json.put(JSON_FILLUP, fillup_date.toString());
        json.put(JSON_DISTANCE, distance.toString());
        json.put(JSON_FUEL_AMT, fuel_amount.toString());
        json.put(JSON_MPG, vehicle_mpg.toString());
        json.put(JSON_PRICE, vehicle_price.toString());
        json.put(JSON_TOTAL, vehicle_total.toString());
//        json.put(JSON_FILLUPLIST, fillupList.toString());
        return json;
    }
}
